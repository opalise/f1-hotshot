ifndef GORILLA_AUDIO_PATH
$(error Please set GORILLA_AUDIO_PATH to the path of your compiled Gorilla Audio installation. See http://code.google.com/p/gorilla-audio/ for more information.)
endif

ifndef PPENGINE_PATH
$(error Please set PPENGINE_PATH to the path of your compiled ppengine installation).
endif

CXX = g++
CXXFLAGS = -O3 -std=c++98 -g -Wall -Wno-narrowing -I src -I $(GORILLA_AUDIO_PATH)/include -I $(PPENGINE_PATH)/src
OSGFLAGS = -losg -losgDB -losgFX -losgGA -losgParticle -losgSim -losgText -losgUtil -losgTerrain -losgManipulator -losgViewer -losgWidget -losgShadow -losgAnimation -losgVolume -lOpenThreads -lpthread
OPENALFLAGS = -lalut -lopenal

TRACK_GEOMETRY_COMMONDEPS = src/track_geometry/points.o src/track_geometry/subpoints.o src/track_geometry/segments.o src/track_geometry/track_geometry.o

SCENE_COMMONDEPS = src/scene/scene.o src/scene/osg/scene.o src/scene/osg/hud_camera.o src/scene/osg/text_matrix.o

INPUT_COMMONDEPS = src/input/input.o src/input/keyboard_queue.o src/input/keyboard_handler.o

AUDIO_COMMONDEPS = src/audio/audio.o src/audio/gorilla/audio.o $(GORILLA_AUDIO_PATH)/bin/linux/Release/libgorilla.a

default: client

clean:
	rm `find src/ -name '*.o'` -rf

client: src/client.c src/client.h $(PPENGINE_COMMONDEPS) $(TRACK_GEOMETRY_COMMONDEPS) $(SCENE_COMMONDEPS) $(INPUT_COMMONDEPS) src/geometry/vertex2d.o $(AUDIO_COMMONDEPS)
	$(CXX) $(CXXFLAGS) src/client.c -o client $(PPENGINE_PATH)/bin/ppengine.a $(TRACK_GEOMETRY_COMMONDEPS) $(SCENE_COMMONDEPS) $(INPUT_COMMONDEPS) src/geometry/vertex2d.o $(AUDIO_COMMONDEPS) -lm $(OSGFLAGS) $(OPENALFLAGS)

#####################################
# geometry                          #
#####################################

GEOMETRY_COMMONDEPS = src/geometry/vertex2d.o

src/geometry/vertex2d.o: src/geometry/vertex2d.c src/geometry/vertex2d.h
	$(CXX) $(CXXFLAGS) -c src/geometry/vertex2d.c -o src/geometry/vertex2d.o

#####################################
# track_geometry                    #
#####################################

src/track_geometry/points.o: src/track_geometry/points.c src/track_geometry/points.h src/geometry/vertex2d.h $(PPENGINE_PATH)/src/ppengine.h
	$(CXX) $(CXXFLAGS) -c src/track_geometry/points.c -o src/track_geometry/points.o

src/track_geometry/subpoints.o: src/track_geometry/subpoints.c src/track_geometry/subpoints.h src/geometry/vertex2d.h
	$(CXX) $(CXXFLAGS) -c src/track_geometry/subpoints.c -o src/track_geometry/subpoints.o

src/track_geometry/segments.o: src/track_geometry/segments.c src/track_geometry/segments.h src/geometry/vertex2d.h
	$(CXX) $(CXXFLAGS) -c src/track_geometry/segments.c -o src/track_geometry/segments.o

src/track_geometry/track_geometry.o: src/track_geometry/track_geometry.c src/track_geometry/track_geometry.h
	$(CXX) $(CXXFLAGS) -c src/track_geometry/track_geometry.c -o src/track_geometry/track_geometry.o

#####################################
# scene                             #
#####################################

src/scene/scene.o: src/scene/scene.c src/scene/scene.h
	$(CXX) $(CXXFLAGS) -c src/scene/scene.c -o src/scene/scene.o

src/scene/osg/scene.o: src/scene/osg/scene.c src/scene/osg/scene.h src/scene/osg/hud_camera.h src/scene/osg/text_matrix.h
	$(CXX) $(CXXFLAGS) -c src/scene/osg/scene.c -o src/scene/osg/scene.o

src/scene/osg/hud_camera.o: src/scene/osg/hud_camera.c src/scene/osg/hud_camera.h
	$(CXX) $(CXXFLAGS) -c src/scene/osg/hud_camera.c -o src/scene/osg/hud_camera.o

src/scene/osg/text_matrix.o: src/scene/osg/text_matrix.c src/scene/osg/text_matrix.h
	$(CXX) $(CXXFLAGS) -c src/scene/osg/text_matrix.c -o src/scene/osg/text_matrix.o

#####################################
# input                             #
#####################################

src/input/input.o: src/input/input.c src/input/input.h $(PPENGINE_PATH)/src/ppengine.h src/input/keyboard_queue.h src/input/keyboard_handler.h src/scene/osg/scene.h
	$(CXX) $(CXXFLAGS) -c src/input/input.c -o src/input/input.o

src/input/keyboard_queue.o: src/input/keyboard_queue.c src/input/keyboard_queue.h
	$(CXX) $(CXXFLAGS) -c src/input/keyboard_queue.c -o src/input/keyboard_queue.o

src/input/keyboard_handler.o: src/input/keyboard_handler.c src/input/keyboard_handler.h
	$(CXX) $(CXXFLAGS) -c src/input/keyboard_handler.c -o src/input/keyboard_handler.o

#####################################
# audio                             #
#####################################

src/audio/audio.o: src/audio/audio.c src/audio/audio.h
	$(CXX) $(CXXFLAGS) -c src/audio/audio.c -o src/audio/audio.o

src/audio/gorilla/audio.o: src/audio/gorilla/audio.c src/audio/gorilla/audio.h
	$(CXX) $(CXXFLAGS) -c src/audio/gorilla/audio.c -o src/audio/gorilla/audio.o


