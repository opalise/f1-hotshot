#ifndef __INPUT_INPUT_H
#define __INPUT_INPUT_H

#include "ppengine.h"

void input_initialise();
void update_engine_state_from_input(struct ppglobals *pp_globals);

#endif

