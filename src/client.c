#include <stdio.h>
#include "client.h"
#include "ppengine.h"
#include "scene/scene.h"
#include "input/input.h"
#include "track_geometry/track_geometry.h"
#include "audio/audio.h"
#include "math.h"

int main(void) {
	struct ppglobals *pp_globals;
	struct movingobject *moving_objects;
	struct hudentry *hud_content;

	pp_engine_initialise();
	pp_engine_register_sound_effect_callback(audio_play_effect);
	pp_globals=pp_get_globals();
	track_geometry_initialise(pp_get_track_curve_data());
	scene_open(pp_get_scenery_objects());
	input_initialise();
	audio_open();
	moving_objects=pp_get_moving_objects();
	hud_content=pp_get_hud_content();
	while (scene_active()) {
		update_engine_state_from_input(pp_globals);
		pp_engine_execute_frame();
		scene_update_display_mode_from_game_state(pp_globals->jump_table_function_number,pp_globals->jump_table_subfunction_number);
		scene_update_moving_objects_and_camera(moving_objects,pp_globals->forward_position,pp_globals->clipped_steering);
		scene_update_hud_content(hud_content);
		scene_update_gantry(pp_globals->gantry_and_starting_lights_appearance);
		audio_update(moving_objects,pp_globals->engine_sound_pitch,pp_globals->engine_sound_active,pp_globals->skidding_volume);
		scene_generate_frame();
	}
	audio_close();
	scene_close();
	track_geometry_terminate();

	return 0;

}

